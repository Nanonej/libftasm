# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aridolfi <aridolfi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/05/16 13:45:38 by aridolfi          #+#    #+#              #
#    Updated: 2019/01/23 14:28:36 by aridolfi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# Executable
NAME 				=	libfts.a
TEST				=	test.out

# Compilation
AC					=	~/.brew/bin/nasm -f macho64
CC					=	cc

# Directories
SRCDIR				=	srcs
OBJDIR				=	objs
TESTDIR				=	tests
TEST_OBJDIR			=	$(TESTDIR)/objs
INCDIR				=	./includes

# Files && Objs

FILES 				=	ft_bzero	\
						ft_cat		\
						ft_isalnum	\
						ft_isalpha	\
						ft_isascii	\
						ft_isdigit	\
						ft_isprint	\
						ft_memcpy	\
						ft_memdup	\
						ft_memset	\
						ft_puts		\
						ft_strcat	\
						ft_strchr	\
						ft_strcmp	\
						ft_strdup	\
						ft_strlen	\
						ft_tolower	\
						ft_toupper	\

TEST_FILES 			=	main		\

OBJ				   :=	$(addsuffix .o, $(FILES))
TEST_OBJ		   :=	$(addsuffix .o, $(TEST_FILES))

# Paths foreach
OBJP 				=	$(addprefix $(OBJDIR)/, $(OBJ))
TEST_OBJP			=	$(addprefix $(TEST_OBJDIR)/, $(TEST_OBJ))

# **************************************************************************** #

# SPECIAL CHARS

LOG_CLEAR			= \033[2K
LOG_UP				= \033[A
LOG_NOCOLOR			= \033[0m
LOG_BOLD			= \033[1m
LOG_UNDERLINE		= \033[4m
LOG_BLINKING		= \033[5m
LOG_BLACK			= \033[1;30m
LOG_RED				= \033[1;31m
LOG_GREEN			= \033[1;32m
LOG_YELLOW			= \033[1;33m
LOG_BLUE			= \033[1;34m
LOG_VIOLET			= \033[1;35m
LOG_CYAN			= \033[1;36m
LOG_WHITE			= \033[1;37m

# Protect

.PHONY				:	clean fclean

# **************************************************************************** #

# RULES

# Main rules
all					:	$(OBJDIR) $(NAME)

test				:	re $(TEST_OBJDIR) $(TEST)

re					:	fclean all

# Compilation rules
$(OBJDIR)			:
						@mkdir -p $@ 2>&-

$(NAME)				:	$(OBJP)
						@ar rc $@ $^
						@ranlib $@
						@echo "--$(LOG_CLEAR)$(LOG_RED)$(NAME)$(LOG_NOCOLOR) ........................ $(LOG_RED)Cake Done$(LOG_NOCOLOR)$(LOG_UP)"

$(OBJDIR)/%.o		:	$(SRCDIR)/%.s
						@echo "--$(LOG_CLEAR)$(LOG_RED)$(NAME)$(LOG_NOCOLOR) ........................ $(LOG_RED)$<$(LOG_NOCOLOR)$(LOG_UP)"
						@$(AC) $^ -o $@

$(TEST_OBJDIR)		:
						@mkdir -p $@ 2>&-

$(TEST)				:	$(TEST_OBJP)
						@$(CC) -o $@ $^ -I. -L. -lfts

$(TEST_OBJDIR)/%.o	:	$(TESTDIR)/%.c
						@echo "--$(LOG_CLEAR)$(LOG_RED)$(NAME)$(LOG_NOCOLOR) ........................ $(LOG_RED)$<$(LOG_NOCOLOR)$(LOG_UP)"
						@$(CC) -c $^ -o $@ -I$(INCDIR)

# Clean rules
clean				:
						@rm -rf $(OBJDIR) $(TEST_OBJDIR)

fclean				:	clean
						@rm -f $(NAME) $(TEST)

# **************************************************************************** #
