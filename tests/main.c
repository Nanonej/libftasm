#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "libfts.h"

int main(void)
{
	char	buf[256];
	char	*tmp;
	int		fd;

	// ft_bzero
	strcpy(buf, "lol");
	printf("\n\x1b[32mft_bzero:\x1b[0m\n%s [ORIGINAL]\n", buf);
	printf("%03d %03d %03d %03d [ORIGINAL]\n", buf[0], buf[1], buf[2],
			buf[3]);
	ft_bzero(NULL, 42);
	printf("%03d %03d %03d %03d [NULL]\n", buf[0], buf[1], buf[2],
			buf[3]);
	ft_bzero(buf, 0);
	printf("%03d %03d %03d %03d [ZERO]\n", buf[0], buf[1], buf[2],
			buf[3]);
	ft_bzero(buf, 1);
	printf("%03d %03d %03d %03d [ONE]\n", buf[0], buf[1], buf[2], buf[3]);
	ft_bzero(buf, 2);
	printf("%03d %03d %03d %03d [TWO]\n", buf[0], buf[1], buf[2], buf[3]);
	ft_bzero(buf, 4);
	printf("%03d %03d %03d %03d [ALL]\n", buf[0], buf[1], buf[2],
			buf[3]);

	// ft_strcat
	strcpy(buf, "lol");
	printf("\n\x1b[32mft_strcat:\x1b[0m\n%s [ORIGINAL]\n", buf);
	printf("%s (buf, 'cats')\n",	ft_strcat(buf, "cats"));
	printf("%s (buf, '!')\n",		ft_strcat(buf, "!"));
	printf("%s (buf, '')\n",		ft_strcat(buf, ""));
	printf("%s (buf, NULL)\n",		ft_strcat(buf, NULL));
	printf("%s (NULL, 'hm...')\n",	ft_strcat(NULL, "hm..."));

	// ft_isalpha
	printf("\n\x1b[32mft_isalpha:\x1b[0m\n");
	printf("%c -> %d\n", '!', ft_isalpha('!'));
	printf("%c -> %d\n", '@', ft_isalpha('@'));
	printf("%c -> %d\n", 'A', ft_isalpha('A'));
	printf("%c -> %d\n", 'F', ft_isalpha('F'));
	printf("%c -> %d\n", 'K', ft_isalpha('K'));
	printf("%c -> %d\n", 'T', ft_isalpha('T'));
	printf("%c -> %d\n", 'Z', ft_isalpha('Z'));
	printf("%c -> %d\n", '[', ft_isalpha('['));
	printf("%c -> %d\n", '`', ft_isalpha('`'));
	printf("%c -> %d\n", 'a', ft_isalpha('a'));
	printf("%c -> %d\n", 'f', ft_isalpha('f'));
	printf("%c -> %d\n", 'k', ft_isalpha('k'));
	printf("%c -> %d\n", 't', ft_isalpha('t'));
	printf("%c -> %d\n", 'z', ft_isalpha('z'));
	printf("%c -> %d\n", '{', ft_isalpha('{'));
	printf("%c -> %d\n", '~', ft_isalpha('~'));

	// ft_isdigit
	printf("\n\x1b[32mft_isdigit:\x1b[0m\n");
	printf("%c -> %d\n", '+', ft_isdigit('+'));
	printf("%c -> %d\n", '/', ft_isdigit('/'));
	printf("%c -> %d\n", '0', ft_isdigit('0'));
	printf("%c -> %d\n", '4', ft_isdigit('4'));
	printf("%c -> %d\n", '2', ft_isdigit('2'));
	printf("%c -> %d\n", '9', ft_isdigit('9'));
	printf("%c -> %d\n", ':', ft_isdigit(':'));
	printf("%c -> %d\n", 'z', ft_isdigit('z'));

	// ft_isalnum
	printf("\n\x1b[32mft_isalnum:\x1b[0m\n");
	printf("%c -> %d\n", '!', ft_isalnum('!'));
	printf("%c -> %d\n", '/', ft_isalnum('/'));
	printf("%c -> %d\n", '0', ft_isalnum('0'));
	printf("%c -> %d\n", '4', ft_isalnum('4'));
	printf("%c -> %d\n", '2', ft_isalnum('2'));
	printf("%c -> %d\n", '9', ft_isalnum('9'));
	printf("%c -> %d\n", ':', ft_isalnum(':'));
	printf("%c -> %d\n", '@', ft_isalnum('@'));
	printf("%c -> %d\n", 'A', ft_isalnum('A'));
	printf("%c -> %d\n", 'F', ft_isalnum('F'));
	printf("%c -> %d\n", 'K', ft_isalnum('K'));
	printf("%c -> %d\n", 'T', ft_isalnum('T'));
	printf("%c -> %d\n", 'Z', ft_isalnum('Z'));
	printf("%c -> %d\n", '[', ft_isalnum('['));
	printf("%c -> %d\n", '`', ft_isalnum('`'));
	printf("%c -> %d\n", 'a', ft_isalnum('a'));
	printf("%c -> %d\n", 'f', ft_isalnum('f'));
	printf("%c -> %d\n", 'k', ft_isalnum('k'));
	printf("%c -> %d\n", 't', ft_isalnum('t'));
	printf("%c -> %d\n", 'z', ft_isalnum('z'));
	printf("%c -> %d\n", '{', ft_isalnum('{'));
	printf("%c -> %d\n", '~', ft_isalnum('~'));

	// ft_isascii
	printf("\n\x1b[32mft_isascii:\x1b[0m\n");
	printf("-1  -> %d\n",		ft_isascii(-1));
	printf("0   -> %d\n",		ft_isascii(0));
	printf("25  -> %d\n",		ft_isascii(25));
	printf("31  -> %d\n",		ft_isascii(31));
	printf("' ' -> %d\n",		ft_isascii(' '));
	printf("%c   -> %d\n", '@',	ft_isascii('@'));
	printf("%c   -> %d\n", '~',	ft_isascii('~'));
	printf("127 -> %d\n",		ft_isascii(127));
	printf("130 -> %d\n",		ft_isascii(130));

	// ft_isprint
	printf("\n\x1b[32mft_isprint:\x1b[0m\n");
	printf("25  -> %d\n",		ft_isprint(25));
	printf("31  -> %d\n",		ft_isprint(31));
	printf("' ' -> %d\n",		ft_isprint(' '));
	printf("%c   -> %d\n", '@',	ft_isprint('@'));
	printf("%c   -> %d\n", '~',	ft_isprint('~'));
	printf("127 -> %d\n",		ft_isprint(127));
	printf("130 -> %d\n",		ft_isprint(130));

	// ft_toupper
	printf("\n\x1b[32mft_toupper:\x1b[0m\n");
	printf("%c -> %c\n", '!', ft_toupper('!'));
	printf("%c -> %c\n", '@', ft_toupper('@'));
	printf("%c -> %c\n", 'A', ft_toupper('A'));
	printf("%c -> %c\n", 'F', ft_toupper('F'));
	printf("%c -> %c\n", 'K', ft_toupper('K'));
	printf("%c -> %c\n", 'T', ft_toupper('T'));
	printf("%c -> %c\n", 'Z', ft_toupper('Z'));
	printf("%c -> %c\n", '[', ft_toupper('['));
	printf("%c -> %c\n", '`', ft_toupper('`'));
	printf("%c -> %c\n", 'a', ft_toupper('a'));
	printf("%c -> %c\n", 'f', ft_toupper('f'));
	printf("%c -> %c\n", 'k', ft_toupper('k'));
	printf("%c -> %c\n", 't', ft_toupper('t'));
	printf("%c -> %c\n", 'z', ft_toupper('z'));
	printf("%c -> %c\n", '{', ft_toupper('{'));
	printf("%c -> %c\n", '~', ft_toupper('~'));

	// ft_tolower
	printf("\n\x1b[32mft_tolower:\x1b[0m\n");
	printf("%c -> %c\n", '!', ft_tolower('!'));
	printf("%c -> %c\n", '@', ft_tolower('@'));
	printf("%c -> %c\n", 'A', ft_tolower('A'));
	printf("%c -> %c\n", 'F', ft_tolower('F'));
	printf("%c -> %c\n", 'K', ft_tolower('K'));
	printf("%c -> %c\n", 'T', ft_tolower('T'));
	printf("%c -> %c\n", 'Z', ft_tolower('Z'));
	printf("%c -> %c\n", '[', ft_tolower('['));
	printf("%c -> %c\n", '`', ft_tolower('`'));
	printf("%c -> %c\n", 'a', ft_tolower('a'));
	printf("%c -> %c\n", 'f', ft_tolower('f'));
	printf("%c -> %c\n", 'k', ft_tolower('k'));
	printf("%c -> %c\n", 't', ft_tolower('t'));
	printf("%c -> %c\n", 'z', ft_tolower('z'));
	printf("%c -> %c\n", '{', ft_tolower('{'));
	printf("%c -> %c\n", '~', ft_tolower('~'));

	// ft_puts
	printf("\n\x1b[32mft_puts:\x1b[0m\n");
	printf("\"\" -> %2d\n", ft_puts(""));
	printf("NULL -> %2d\n", ft_puts(NULL));
	printf("lol  -> %2d\n", ft_puts("lol"));
	printf("cats -> %2d\n", ft_puts("cats"));

	// ft_strlen
	printf("\n\x1b[32mft_strlen:\x1b[0m\n");
	printf("%s -> %zu\n"	,	NULL		,	ft_strlen(NULL));
	printf("\"\" -> %zu\n"	,					ft_strlen(""));
	printf("%s -> %zu\n"	,	"lol"		,	ft_strlen("lol"));
	printf("%s -> %zu\n"	,	"cats"		,	ft_strlen("cats"));
	printf("%s -> %zu\n"	,	"lolcats"	,	ft_strlen("lolcats"));
	printf("%s -> %zu\n"	,	"lolcats!"	,	ft_strlen("lolcats!"));

	// ft_memset
	memset(buf, 0, 256);
	printf("\n\x1b[32mft_memset:\x1b[0m\n'%s' [ORIGINAL]\n", buf);
	printf("'%s' (NULL, 42, 42)\n"	,	ft_memset(NULL, 42, 42));
	printf("'%s' (buf, '$', 25)\n"	,	ft_memset(buf, '$', 25));
	printf("'%s' (buf, '-', 20)\n"	,	ft_memset(buf, '-', 20));
	printf("'%s' (buf, '=', 10)\n"	,	ft_memset(buf, '=', 10));
	printf("'%s' (buf, 'H', 5)\n"	,	ft_memset(buf, 'H', 5));
	printf("'%s' (buf, 'B', 1)\n"	,	ft_memset(buf, 'B', 1));
	printf("'%s' (buf, '~', 0)\n"	,	ft_memset(buf, '~', 0));

	// ft_memcpy
	memset(buf, 0, 256);
	printf("\n\x1b[32mft_memcpy:\x1b[0m\n'%s' [ORIGINAL]\n", buf);
	printf("'%s' (NULL, buf, 42)\n"					,
		ft_memcpy(NULL, buf, 42));
	printf("'%s' (buf, NULL, 42)\n"					,
		ft_memcpy(buf, NULL, 42));
	printf("'%s' (buf, 'YOLOSWAG!', 9)\n"			,
		ft_memcpy(buf, "YOLOSWAG!", 9));
	printf("'%s' (buf, 'BLUE RACECAR', 6)\n"		,
		ft_memcpy(buf, "BLUE RACECAR", 6));
	printf("'%s' (buf, 'TRUTH AND JUSTICE', 2)\n"	,
		ft_memcpy(buf, "TRUTH AND JUSTICE", 2));
	printf("'%s' (buf, 'NOTHING', 0)\n"				,
		ft_memcpy(buf, "NOTHING", 0));

	// ft_strdup
	printf("\n\x1b[32mft_strdup:\x1b[0m\n");
	printf("'%s' (NULL)\n"				, (tmp = ft_strdup(NULL)));
	printf("'%s' ('')\n"				, (tmp = ft_strdup("")));
	free(tmp);
	printf("'%s' ('YOLO!')\n"			, (tmp = ft_strdup("YOLO!")));
	free(tmp);
	printf("'%s' ('ft_strdup work')\n"	, (tmp = ft_strdup("ft_strdup work")));
	free(tmp);

	// ft_cat
	printf("\x1b[32mft_cat Makefile:\x1b[0m\n");
	fd = open("./Makefile", O_RDONLY);
	ft_cat(fd);
	close(fd);
	printf("\x1b[32mft_cat stdin: (ctrl-D to quit)\x1b[0m\n");
	ft_cat(0);

	//--------------------------------BONUS--------------------------------//

	// ft_strcmp
	printf("\n\x1b[32m\n### BONUS ###\n\nft_strcmp:\x1b[0m\n");
	printf("%d ('', '')\n"									,
			ft_strcmp("", ""));
	printf("%d ('42', '42')\n"								,
			ft_strcmp("42", "42"));
	printf("%d ('AAA', 'BBB')\n"							,
			ft_strcmp("AAA", "BBB"));
	printf("%d ('1', '22')\n"								,
			ft_strcmp("1", "22"));
	printf("%d ('11', '2')\n"								,
			ft_strcmp("11", "2"));
	printf("%d ('1234', '123')\n"							,
			ft_strcmp("1234", "123"));
	printf("%d ('123', '1234')\n"							,
			ft_strcmp("123", "1234"));
	printf("%d ('12345', '12355')\n"						,
			ft_strcmp("12345", "12355"));
	printf("%d ('Equal States', 'Equal Factions')\n"		,
			ft_strcmp("Equal States", "Equal Factions"));
	printf("%d ('Equal Factions', 'Equal Factions')\n"		,
			ft_strcmp("Equal Factions", "Equal Factions"));

	// ft_memdup
	printf("\n\x1b[32mft_memdup:\x1b[0m\n");
	tmp = ft_memdup(NULL, 42);
	printf("'%s' (NULL, 42)\n", tmp);
	tmp = ft_memdup("", 0);
	printf("ZERO ('', 0)\n");
	free(tmp);
	tmp = ft_memdup("", 1);
	printf("'%s' ('', 1)\n", tmp);
	free(tmp);
	tmp = ft_memdup("ft_memdup work", 10);
	tmp[12] = 0;
	printf("'%s' ('ft_memdup work', 10)\n", tmp);
	free(tmp);
	tmp = ft_memdup("ft_memdup work", 15);
	printf("'%s' ('ft_memdup work', 15)\n", tmp);
	free(tmp);

	// ft_strchr
	printf("\n\x1b[32mft_strchr:\x1b[0m\n");
	printf("'%s' (NULL, '?')\n"				,	ft_strchr(NULL, '?'));
	printf("'%s' ('', '?')\n"				,	ft_strchr("", '?'));
	printf("'%s' ('', '\\0')\n"				,	ft_strchr("", '\0'));
	printf("'%s' ('string', 'c')\n"			,	ft_strchr("string", 'c'));
	printf("'%s' ('string', 'r')\n"			,	ft_strchr("string", 'r'));
	printf("'%s' ('magic trick', '\\0')\n"	,	ft_strchr("magic trick", '\0'));
	printf("'%s' ('Did it work?', '?')\n"	,	ft_strchr("Did it work?", '?'));
	printf("'%s' ('Yup it work!', 'w')\n"	,	ft_strchr("Yup it work!", 'w'));

	return 0;
}
