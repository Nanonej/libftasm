section .text
	global _ft_isprint

_ft_isprint:		; int isprint(int c);
	cmp edi, 32
	jl false			; false if < ' '
	cmp edi, 126
	jg false			; false if > '~'
	mov eax, 1
	ret					; else true

false:
	mov eax, 0
	ret
