section .text
	global _ft_memdup
	extern _malloc

_ft_memdup:		; void *ft_memdup(const void *src, size_t len);
	push rbp
	mov rbp, rsp	; enter
	cmp rdi, 0
	je exit
	push rdi
	push rsi		; save args
	mov rdi, rsi	; malloc(len)
	call _malloc
	cmp rax, 0
	je alloc_error
	pop rcx 		; pop len
	mov rdi, rax	; attribute *ptr for dup
	pop rsi			; pop string
	cld
	rep movsb		; dup mem
	jmp end

alloc_error:
	pop rax

exit:
	xor rax, rax

end:
	leave
	ret
