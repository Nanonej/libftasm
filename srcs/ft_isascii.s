section .text
	global _ft_isascii

_ft_isascii:		; int isascii(int c);
	cmp edi, 0
	jl false			; false if < 'nul'
	cmp edi, 127
	jg false			; false if > 'del'
	mov eax, 1
	ret					; else true

false:
	mov eax, 0
	ret
