section .text
	global _ft_strcmp

_ft_strcmp:		; int strcmp(const char *s1, const char *s2);
	cmp rdi, 0
	je error
	cmp rsi, 0
	je error				; handle null strings
	mov r10, rdi
	xor rcx, rcx
	not rcx
	xor rax, rax
	cld
	repne scasb
	not rcx
	mov rdi, r10
	repe cmpsb
	mov al, byte[rdi-1]
	xor r10, r10
	mov r10b, byte[rsi-1]	; rewind of on char coth strings
	sub rax, r10			; calculate return value
	ret

error:
	xor rax, rax
	ret
