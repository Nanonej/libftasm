section .rodata
	endl db 10
	null db "(null)", 10

section .text
	global _ft_puts
	extern _ft_strlen

_ft_puts:	; int puts(const char *s);
	cmp rdi, 0
	je null_str
	mov rsi, rdi		; write(?, rsi, ?);
	call _ft_strlen
	mov rdx, rax		; write(?, rsi, rdx)
	mov rdi, 1			; write(rdi, rsi, rdx)
	mov rax, 0x2000004	; write @ 4
	syscall
	cmp rax, 0			; check write return
	jl error

	mov rdi, 1
	lea rsi, [rel endl]	; write new line
	mov rdx, 1
	mov rax, 0x2000004
	syscall
	cmp rax, 0
	jl error

	mov eax, 10
	ret

null_str:
	mov rdi, 1
	lea rsi, [rel null] ; write "(null)\n"
	mov rdx, 7
	mov rax, 0x2000004
	syscall
	cmp rax, 0
	jl error
	mov eax, 10
	ret

error:
	mov eax, -1
	ret
