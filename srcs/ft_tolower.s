section .text
	global _ft_tolower

_ft_tolower:		; int tolower(int c);
	cmp edi, 90
	jg end			; end if > 'Z'
	cmp edi, 65
	jl end			; end if < 'A'
	xor edi, 32		; upcase if 'A' <= edi <= 'Z'

end:
	mov eax, edi
	ret
