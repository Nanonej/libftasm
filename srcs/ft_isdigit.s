section .text
	global _ft_isdigit

_ft_isdigit:		; int isdigit(int c);
	cmp edi, 48
	jl false			; false if < '0'
	cmp edi, 57
	jg false			; false if > '9'
	mov eax, 1
	ret					; else true

false:
	mov eax, 0
	ret
