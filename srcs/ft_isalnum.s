section .text
	global _ft_isalnum
	extern _ft_isalpha
	extern _ft_isdigit

_ft_isalnum:		; int isalnum(int c);
	call _ft_isalpha
	cmp eax, 1
	je true
	call _ft_isdigit
	cmp eax, 1
	je true
	mov eax, 0
	ret

true:
	ret
