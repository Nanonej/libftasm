section .data
	buf: times 256 db 0

section .text
	global _ft_cat

_ft_cat:	; void	ft_cat(int fd);
	push rbp
	mov rbp, rsp		; enter
	cmp edi, 0
	jl end
	cmp edi, 255
	jg end				; check fd validity

read_loop:
	push rdi
	lea rsi, [rel buf]
	mov rdx, 256
	mov rax, 0x2000003	; read @ 3
	syscall
	jc end_loop			; handle error
	cmp rax, 0			; check EOL
	je end_loop

	mov rdi, 1
	lea rsi, [rel buf]
	mov rdx, rax
	mov rax, 0x2000004	; write @ 4
	syscall
	jc end_loop
	pop rdi
	jmp read_loop

end_loop:
	pop rdi

end:
	leave
	ret
