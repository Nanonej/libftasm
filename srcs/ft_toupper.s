section .text
	global _ft_toupper

_ft_toupper:		; int toupper(int c);
	cmp edi, 122
	jg end			; end if > 'z'
	cmp edi, 97
	jl end			; end if < 'a'
	and edi, 95		; upcase if 'a' <= edi <= 'z'

end:
	mov eax, edi
	ret
