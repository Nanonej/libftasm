section .text
	global _ft_bzero

_ft_bzero:				; void bzero(void *s, size_t n);
	cmp rdi, 0
	je exit				; ret if s == null
	xor al, al			; set al = \0
	mov rcx, rsi
	cld
	rep stosb

exit:
	ret
