section .text
	global _ft_strchr

_ft_strchr:		; char *strchr(const char *s, int c);
	cmp rdi, 0
	je exit
	mov r10, rdi
	xor rcx, rcx
	not rcx
	xor al, al
	repne scasb
	not rcx
	mov rdi, r10
	mov al, sil
	repne scasb
	cmp al, byte[rdi-1]
	jne exit
	dec rdi
	mov rax, rdi
	ret

exit:
	xor rax, rax
	ret
