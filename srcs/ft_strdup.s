section .text
	global _ft_strdup
	extern _ft_strlen
	extern _malloc

_ft_strdup:		; char *strdup(const char *s1);
	push rbp
	mov rbp, rsp	; enter
	cmp rdi, 0
	je exit
	push rdi		; save string
	call _ft_strlen
	mov rcx, rax
	push rcx		; push len to align on 16
	mov rdi, rcx	; malloc(len)
	call _malloc
	cmp rax, 0
	je alloc_error
	pop rcx 		; pop len
	mov rdi, rax	; attribute *ptr for dup
	pop rsi			; pop string
	cld
	rep movsb		; dup string
	jmp end

alloc_error:
	pop rax

exit:
	xor rax, rax

end:
	leave
	ret
