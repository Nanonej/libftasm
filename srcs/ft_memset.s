section .text
	global _ft_memset

_ft_memset:				; void *memset(void *b, int c, size_t len);
	cmp rdi, 0
	je exit
	mov r10, rdi
	mov al, sil
	mov rcx, rdx
	cld
	rep stosb
	mov rax, r10
	ret

exit:
	mov rax, rdi
	ret
