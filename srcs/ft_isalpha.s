section .text
	global _ft_isalpha

_ft_isalpha:		; int isalpha(int c);
	cmp edi, 122
	jg false			; false if > 'z'
	mov r10d, edi		; save edi
	and r10d, 95		; upcase if 'a' <= edi <= 'z'
	cmp r10d, 65
	jl false			; false if < 'A'
	cmp r10d, 90
	jg false			; false if > 'Z'
	mov eax, 1
	ret					; else true

false:
	mov eax, 0
	ret
