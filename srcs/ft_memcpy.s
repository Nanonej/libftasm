section .text
	global _ft_memcpy

_ft_memcpy:		; void *memcpy(void *restrict dst, const void *restrict src, size_t n);
	mov r10, rdi
	cmp rdi, 0
	je end
	cmp rsi, 0
	je end
	mov rcx, rdx
	cld
	rep movsb

end:
	mov rax, r10
	ret
