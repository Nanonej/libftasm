section .text
	global _ft_strlen

_ft_strlen:	; size_t strlen(const char *s);
	cmp rdi, 0
	je exit
	xor rcx, rcx
	not rcx
	xor al, al
	cld
	repne scasb
	not rcx
	dec rcx
	mov rax, rcx
	ret

exit:
	mov rax, 0
	ret
