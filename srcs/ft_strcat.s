section .text
	global _ft_strcat

_ft_strcat:		; char *strcat(char *restrict s1, const char *restrict s2);
	cmp rdi, 0
	je exit				; ret if s1 == null
	cmp rsi, 0
	je exit				; ret if s2 == null
	mov r11, rdi		; save *s1
	xor al, al			; set al = '\0'
	mov rcx, -1			; ignore iterator
	cld
	repne scasb
	dec rdi
	mov r10, rdi		; loop to the '\0' of *s1 and save
	mov rdi, rsi
	xor rcx, rcx
	not rcx
	cld
	repne scasb
	not rcx				; loop and set rcx after *s2 '\0'
	mov rdi, r10
	rep movsb
	mov rax, r11		; copy s2 to s1
	ret

exit:
	mov rax, rdi
	ret
